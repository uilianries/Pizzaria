/*!
 * @file TestCarrinhoDeCompras.java
 * @brief Definição para o case de teste para Carrinho de compras
 * 
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.pizza;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

/*!
 * @brief Valida funcionalidades do carrinho
 */
public class TestCarrinhoDeCompras {

	/*!
	 * @brief Limpa tabela de sabores
	 */
	@After
	public void TearDown() {
		Pizza.zeraIngredientes();
	}

	/*!
	 * @brief Adiciona uma pizza no carrinho
	 */
	@Test
	public void testAdicionaPizza() {
		Pizza pizza = new Pizza();
		pizza.adicionaIngrediente("Mussarela");
		
		CarrinhoDeCompras carrinho = new CarrinhoDeCompras();
		carrinho.adicionaPizza(pizza);
	}

	/*!
	 * @brief Aguarda receber execeção ao adicionar pizza vazia
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testAdicionaPizzaVazia() {
		Pizza pizza = new Pizza();		
		CarrinhoDeCompras carrinho = new CarrinhoDeCompras();
		carrinho.adicionaPizza(pizza);
	}

	/*!
	 * @brief Realiza um pedido de pizza completo
	 */
	@Test
	public void testValorTotal() {
		Pizza pizza = new Pizza();
		pizza.adicionaIngrediente("Mussarela");
		
		CarrinhoDeCompras carrinho = new CarrinhoDeCompras();
		carrinho.adicionaPizza(pizza);
		assertTrue(Pizza.Valor.PEQUENA == carrinho.valorTotal());
		pizza.adicionaIngrediente("Mussarela");
		assertTrue(Pizza.Valor.PEQUENA == carrinho.valorTotal());
		pizza.adicionaIngrediente("Mussarela");
		assertTrue(Pizza.Valor.MEDIA == carrinho.valorTotal());
		
		Pizza pizzaGrande = new Pizza();
		for (int i = 0; i < 8; i++) {
			pizzaGrande.adicionaIngrediente("Mussarela");
		}
		carrinho.adicionaPizza(pizzaGrande);
		assertTrue(Pizza.Valor.GRANDE == pizzaGrande.getPreco());
		assertTrue((pizza.getPreco() + pizzaGrande.getPreco()) == carrinho.valorTotal());
	}

}

/*!
 * @file TestPizza.java
 * @brief Valida o comportamento da class Pizza
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.pizza;

import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestPizza {

	/*!
	 * @brief Limpa tabela após cada caso de teste
	 */
	@After
	public void TearDown() {
		Pizza.zeraIngredientes();
	}

	/*!
	 * @brief Aguarda lançar exceção, quando pizza é preenchida com valor vazio
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testPizzaVazia() {
		Pizza pizza = new Pizza();
		pizza.adicionaIngrediente("");
	}

	/*!
	 * @brief Compara relação Tamanho x Valor
	 */
	@Test
	public void testPizzaValor() {
		Pizza pizza = new Pizza();
		
		assertTrue(0 == pizza.getPreco());
		pizza.adicionaIngrediente("Mussarela");
		assertTrue(Pizza.Valor.PEQUENA == pizza.getPreco());
		pizza.adicionaIngrediente("Calabresa");
		assertTrue(Pizza.Valor.PEQUENA == pizza.getPreco());
		pizza.adicionaIngrediente("Palmito");
		assertTrue(Pizza.Valor.MEDIA == pizza.getPreco());
		pizza.adicionaIngrediente("Mussarela");
		assertTrue(Pizza.Valor.MEDIA == pizza.getPreco());
		pizza.adicionaIngrediente("Mussarela");
		assertTrue(Pizza.Valor.MEDIA == pizza.getPreco());
		pizza.adicionaIngrediente("Mussarela");
		assertTrue(Pizza.Valor.GRANDE == pizza.getPreco());
		pizza.adicionaIngrediente("Mussarela");
		assertTrue(Pizza.Valor.GRANDE == pizza.getPreco());
	}

	/*!
	 * @brief Valida comportamento de flush da tabela
	 */
	@Test
	public void testZeraPizza() {
		Pizza pizza = new Pizza(new String[]{"Mussarela", "Morango", "Chocolate", "Milho"});

		assertTrue(pizza.getPreco() == Pizza.Valor.MEDIA);
		assertTrue(pizza.getQuantidadeSabores() == 4);

		Pizza.zeraIngredientes();
		assertTrue(pizza.getPreco() == 0);
		assertTrue(pizza.getQuantidadeSabores() == 0);
	}
}

/*!
 * @file Pizza.java
 * @brief Representação para uma pizza
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.pizza;

import java.util.HashMap;
import java.util.Map;
import java.lang.Integer;

/*!
 * @brief Pizza com sabores
 */
public class Pizza {

    /*! Total de ingredientes utilizados para todas as pizzas */
    static private HashMap<String, Integer> ingredientesUtilizados = new HashMap<String, Integer>();
    /*! Quantidade de sabores na mesma pizza */
    private int quantidadeSabores;

    /*!
     * Cria pizza com sabores definidos
     */
    public Pizza(String[] sabores) {
        for (String sabor : sabores) {
            adicionaIngrediente(sabor);
        }
    }

    /*!
     * @brief Construstor padrão
     */
    public Pizza() {}

    /*!
     * @brief Definição para tamanho de pizza
     */
    public static class Tamanho {
        static final int PEQUENA = 2;
        static final int MEDIA = 5;
        static final int GRANDE = 10;
    }

    /*!
     * @brief Definição para valores de pizza
     */
    public static class Valor {
        static final int PEQUENA = 15;
        static final int MEDIA = 20;
        static final int GRANDE = 23;
    }
    
    /*! Relação de valor por tamanho de pizza */
    private static final Map<Integer, Integer> tabelaPreco = new HashMap<Integer, Integer>() {{
        put(Tamanho.PEQUENA, Valor.PEQUENA);
        put(Tamanho.MEDIA, Valor.MEDIA);
        put(Tamanho.GRANDE, Valor.GRANDE);
    }};
    
    /*!
     * @brief Imprime relação de todos os ingredientes utilizados
     */
    static public void imprimeTotalIngredientes() {
    	System.out.println("Total de ingredientes utilizados:");
    	for (Map.Entry<String, Integer> it : ingredientesUtilizados.entrySet()) {
    		System.out.println("Nome: " + it.getKey() + "\tQuantidade: " + it.getValue());
    	}
    }
    
    /*!
     * @brief Atualiza tabela total de ingredientes
     * @param ingrediente Ingrediente para ser inserido na tabela
     */
    private void contabilizaIngrediente(String ingrediente) {
        int quantidade = 0;
        if (ingredientesUtilizados.containsKey(ingrediente)) {
        	quantidade = ingredientesUtilizados.get(ingrediente);
        }
        ingredientesUtilizados.put(ingrediente, quantidade + 1);
    }
    
    /*!
     * @brief Adiciona um novo ingrediente na tabela e incrementa o valor da pizza
     * @param ingrediente  Nome do ingrediente a ser adicionado
     * @exception IllegalArgumentException se o ingrediente for vazio
     */
    public void adicionaIngrediente(String ingrediente) {
        if ("".equals(ingrediente)) {
            throw new IllegalArgumentException("Ingrediente não pode ser vazio.");
        }

        contabilizaIngrediente(ingrediente);
        quantidadeSabores++;
    }

    /*!
     * @brief Zera mapa de ingredientes associados
     */
    public static void zeraIngredientes() {
        ingredientesUtilizados.clear();
    }
    
    /*!
     * @brief Recupera valor da pizza atualizado, baseado no tamanho
     * @return Valor total da pizza
     */
    public double getPreco() {
        if (ingredientesUtilizados.isEmpty()) {
            this.quantidadeSabores = 0;
            return 0;
        }

    	int tamanho;
    	if (this.quantidadeSabores <= Tamanho.PEQUENA) {
    		tamanho = Tamanho.PEQUENA;
    	} else if (this.quantidadeSabores <= Tamanho.MEDIA) {
    		tamanho = Tamanho.MEDIA;
    	} else {
    		tamanho = Tamanho.GRANDE;
    	}
    	
    	return tabelaPreco.get(tamanho);
    }
    
    /*!
     * @brief Retorna o quantidade de sabores da pizza
     */
    public int getQuantidadeSabores() {
    	return this.quantidadeSabores;
    }
}

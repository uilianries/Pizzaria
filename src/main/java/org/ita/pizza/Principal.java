/*!
 * @file Principal.java
 * @brief Execução do fluxo de uma compra
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.pizza;

import java.util.Arrays;
import java.util.List;

/*!
 * @brief Realiza a compra de 3 pizzas 
 */
public class Principal {
	
	/*!
	 * @brief Instancia uma nova pizza e adiciona no carrinho
	 * @param carrinho  Carrinho que recebe a pizza
	 * @param sabores   Sabores que acompanham a nova pizza
	 */
	static public void ComprarPizza(CarrinhoDeCompras carrinho, List<String> sabores) {
		Pizza pizza = new Pizza();
		for (String sabor : sabores) {
			pizza.adicionaIngrediente(sabor);
		}
		carrinho.adicionaPizza(pizza);
	}

	public static void main(String[] args) {
		CarrinhoDeCompras carrinho = new CarrinhoDeCompras();
		
		ComprarPizza(carrinho, Arrays.asList("Mussarela", "Calabresa"));
		ComprarPizza(carrinho, Arrays.asList("Frango", "Milho", "Palmito", "Calabresa"));
		ComprarPizza(carrinho, Arrays.asList("Mussarela", "Milho", "Chocolate", "Calabresa", "Atum", "Carne"));
		
		System.out.println("Total do valor da compra (R$): " + carrinho.valorTotal());
		Pizza.imprimeTotalIngredientes();
	}

}

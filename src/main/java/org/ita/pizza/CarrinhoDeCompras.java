/*!
 * @file CarrinhoDeCompras.java
 * @brief Definição para o Carrinho de compras
 * 
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.pizza;

import java.util.ArrayList;

/*!
 * @brief Carrinho que comporta um pedido de pizza
 */
public class CarrinhoDeCompras {
	/*! Pizzas presentes no carrinho */
	private ArrayList<Pizza> pizzas = new ArrayList<Pizza>();
	
	/*!
	 * @brief Adiciona uma nova pizza no carrinho
	 * @param pizza Produto para ser adiciona
     * @exception IllegalArgumentException se a pizza não possuir ingredientes
	 */
	public void adicionaPizza(Pizza pizza) {
		if (pizza.getQuantidadeSabores() == 0) {
			throw new IllegalArgumentException("Pizza deve conter pelo menos 1 sabor.");
		}
		
		pizzas.add(pizza);
	}
	
	/*!
	 * @brief Calcula o valor total das pizzas no carrinho
	 * @return Soma de todas as pizzas
	 */
	public double valorTotal() {
		double total = 0;
		
		for (Pizza pizza : pizzas) {
			total += pizza.getPreco();
		}
		
		return total;
	}

}
